package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

public class JSONWalletDeserializerDAImpl implements DirectAccessDatabaseDeserializer {

    private Set<DigitalBadge> metas;
    private static final Logger LOG = LogManager.getLogger(JSONWalletDeserializerDAImpl.class);
    private OutputStream sourceOutpStream;

    public JSONWalletDeserializerDAImpl(OutputStream imageStream,Set<DigitalBadge>metas){
        this.sourceOutpStream=imageStream;
        this.metas=metas;
    }
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {

    }

    @Override
    public <K extends InputStream> K getDeserializingStream(String data) throws IOException {
        return null;
    }

    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException {

        long pos = targetBadge.getMetadata().getWalletPosition();
        media.getChannel().seek(pos);

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

        String data = media.getEncodedImageReader(false).readLine();
        String badgeData = data.split(",\\{\"payload")[0].split(".*badge\":")[1];
        DigitalBadge badge = objectMapper.readValue(badgeData,DigitalBadge.class);

        String encodedImageData = data.split(",\\{\"payload\":")[1].split("\"\\}]")[0];
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(encodedImageData).transferTo(os);
        }
        targetBadge.setSerial(badge.getSerial());
        targetBadge.setBegin(badge.getBegin());
        targetBadge.setEnd(badge.getEnd());
    }


    @Override
    public <T extends OutputStream> T getSourceOutputStream() {
        return null;
    }

    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {

    }
}
