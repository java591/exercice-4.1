package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Set;

public class JSONBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
    private static final Logger LOG = LogManager.getLogger(JSONBadgeWalletDAOImpl.class);

    private final File walletDB;

    public JSONBadgeWalletDAOImpl(String dbPath) {
        this.walletDB = new File(dbPath);
    }

    @Override
    public void addBadge(File image) throws IOException {
        LOG.error("Non supporté");
        throw new IOException("Méthode non supportée, utilisez plutôt addBadge(DigitalBadge badge)");
    }

    @Override
    public void addBadge(DigitalBadge badge) throws IOException {
        try (WalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDB, "rw"))) {
            ImageStreamingSerializer serializer = new JSONWalletSerializerDAImpl(getWalletMetadata());
            serializer.serialize(badge, media);
        }
    }

    @Override
    public void getBadge(OutputStream imageStream) throws IOException {
        try (WalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDB, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }

    @Override
    public Set<DigitalBadge> getWalletMetadata() throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDB, "r"))) {
            return new MetadataDeserializerJSONImpl().deserialize(media);
        }
    }

    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException {
        Set<DigitalBadge> metas = this.getWalletMetadata();
        try (WalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDB, "rw"))) {
            new JSONWalletDeserializerDAImpl(imageStream, metas).deserialize(media, meta);
        }

    }
}
