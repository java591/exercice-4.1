package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

public class JSONWalletSerializerDAImpl extends AbstractStreamingImageSerializer<DigitalBadge, JSONWalletFrame> {

    private Set<DigitalBadge>metas;
    public JSONWalletSerializerDAImpl(Set<DigitalBadge>metas){
        this.metas=metas;
    }

    @Override
    public InputStream getSourceInputStream(DigitalBadge source) throws IOException {
        return new FileInputStream(source.getBadge());
    }

    @Override
    public OutputStream getSerializingStream(JSONWalletFrame media) throws IOException {
        return null;
    }
}
